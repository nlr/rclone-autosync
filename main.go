package main

import (
	"context"
	"errors"
	"flag"
	"fmt"
	"io/fs"
	"log"
	"os"
	"os/exec"
	"os/signal"
	"path/filepath"
	"sync"
	"syscall"
	"time"

	"github.com/bep/debounce"
	"github.com/fsnotify/fsnotify"
)

var logger = log.New(os.Stdout, "", 0)
var errLogger = log.New(os.Stderr, "", 0)

type Config struct {
	PeriodicSyncDelay time.Duration
	DebounceDelay     time.Duration
	LocalPath         string
	RemotePath        string
}

func doRcloneBisync(config Config, resync bool) {
	resyncFlag := "--resync=false"
	if resync {
		resyncFlag = "--resync=true"
	}

	cmdSync := exec.Command("rclone", "bisync", resyncFlag, config.LocalPath, config.RemotePath)
	_, errSync := cmdSync.Output()

	if errSync == nil {
		logger.Println("sync successful.")
	} else {
		errLogger.Println("error syncing:", errSync)
		if exitErr, ok := errSync.(*exec.ExitError); ok {
			errLogger.Writer().Write(exitErr.Stderr)
			if !resync && exitErr.ExitCode() == 2 {
				errLogger.Println("resyncing…")
				doRcloneBisync(config, true)
			}
		}
	}
}

func doSync(config Config) {
	logger.Printf("start syncing…")
	doRcloneBisync(config, false)
}

func watch(ctx context.Context, config Config) {
	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		logger.Fatal(err)
	}
	defer watcher.Close()

	isSyncing := false
	doSyncDebouncer := debounce.New(config.DebounceDelay)
	mutex := sync.Mutex{}
	debouncedDoSync := func() {
		doSyncDebouncer(func() {
			mutex.Lock()
			defer mutex.Unlock()

			isSyncing = true
			defer func() { isSyncing = false }()

			doSync(config)
		})
	}

	err = filepath.WalkDir(config.LocalPath, func(path string, dirEntry fs.DirEntry, err error) error {
		if err != nil {
			return err
		}

		if dirEntry.IsDir() {
			if err := watcher.Add(path); err != nil {
				return err
			}
		}

		return nil
	})
	if err != nil {
		errLogger.Fatalln("couldn't init watcher", err)
	}

	doSync(config)
L:
	for {
		var periodicSyncCh <-chan time.Time
		if config.PeriodicSyncDelay != 0 {
			periodicSyncCh = time.After(config.PeriodicSyncDelay)
		}

		select {
		case event := <-watcher.Events:
			if isSyncing {
				continue
			}
			logger.Println("file change detected:", event.Name)
			debouncedDoSync()
		case err := <-watcher.Errors:
			errLogger.Println("watcher error:", err)
		case <-periodicSyncCh:
			debouncedDoSync()
		case <-ctx.Done():
			break L
		}
	}
}

func createDirIfNotExists(path string) error {
    err := os.Mkdir(path, 0744)

    if err == nil {
        return nil
    }

    if os.IsExist(err) {
        fileInfo, err := os.Stat(path)
        if err != nil {
            return err
        }

        if !fileInfo.IsDir() {
            return errors.New("path already exists and isn't a directory")
        }

	// path already exists but is indeed a directory
        return nil
    }

    return err
}

func main() {
	autoSyncDelay := flag.Int("periodic-sync-delay", 600, "At least sync every N seconds.")
	syncDelay := flag.Int("debounce-delay", 10, "Wait N seconds before syncing when a change is detected.")
	flag.Usage = func() {
		fmt.Fprintf(
			os.Stderr,
			"Usage: %s [-periodic-sync-delay=N] [-debounce-delay=N] <local_path> <rclone_remote>:<rclone_path>\n",
			os.Args[0],
		)
		flag.PrintDefaults()
	}
	flag.Parse()

	if flag.NArg() != 2 {
		flag.Usage()
		os.Exit(1)
	}

	config := Config{
		PeriodicSyncDelay: time.Duration(*autoSyncDelay) * time.Second,
		DebounceDelay:     time.Duration(*syncDelay) * time.Second,
		LocalPath:         flag.Arg(0),
		RemotePath:        flag.Arg(1),
	}

	// Create local directory if not exists
	if err := createDirIfNotExists(config.LocalPath); err != nil {
		errLogger.Println("couldn't create local directory")
		panic(err)
	}

	// Handle OS signals
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)
	ctx, cancelCtx := context.WithCancel(context.Background())
	go func() {
		<-sigs
		cancelCtx()
	}()

	wg := sync.WaitGroup{}

	wg.Add(1)
	go func() {
		defer wg.Done()
		watch(ctx, config)
	}()

	wg.Wait()
}
