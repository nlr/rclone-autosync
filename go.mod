module gitlab.com/nlr/rclone-autosync

go 1.18

require (
	github.com/bep/debounce v1.2.0
	github.com/fsnotify/fsnotify v1.5.1
)

require golang.org/x/sys v0.0.0-20210630005230-0f9fa26af87c // indirect
