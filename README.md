# rclone-autosync

Dummy go program for continuous bi-directional syncing of a rclone remote. It's
just good-enough for my personal use-case, don't use it for anything serious.
It's racy and you might end up overwriting your files.
